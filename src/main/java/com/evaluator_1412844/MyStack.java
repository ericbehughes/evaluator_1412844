/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluator_1412844;

/**
 *
 * @author ehugh
 */

/**
 * find a way to add to the end of the array 
 * 
 * when popping its the last element of the array 
 * @author ehugh
 * @param <T> 
 */
public interface MyStack<T> {

    public void push(T t);

    public T pop();

    public T peek();

    public int size();

}
