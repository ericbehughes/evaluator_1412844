/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluator_1412844;


/**
 *
 * @author ehugh
 */
public class DynamicArray<T> implements MyQueue<T>, MyStack<T> {

    private Object[] array;
    private int capacity = 0; // Actual array size
    private int nItems = 0; // num of items

    public DynamicArray() {
        this(16);

    }

    // Size is the number of elements being used and capacity is the total number of elements available. 
    // When you create an instance of your dynamic array 
    // the only method you can use is add(T t). Once you have a size of 1 or more then you can use the pos value.
    public DynamicArray(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + capacity);
        }
        this.capacity = capacity;
        array = new Object[capacity];
        nItems = 0;

        
    }

    public int size() {
        return nItems;
    }

    /**
     * isEmpty - returns true if the stack is empty, and false otherwise
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * isFull - returns true if the stack is full, and false otherwise
     */
    public boolean isFull() {
        return (nItems == array.length - 1);
    }

    public T get(int index) {
        return (T) array[index];
    }

    public void set(int index, T elem) {
        array[index] = elem;
    }

    public void add(T elem) {

        // Time to resize
        if (nItems + 1 >= capacity) {
            if (capacity == 0) {
                capacity = 1;
            } else {
                capacity *= 2; // double the size
            }
            Object[] tempArray = new Object[capacity];
            for (int i = 0; i < nItems; i++) {
                tempArray[i] = array[i];
            }
            array = tempArray;
        }

        array[nItems++] = elem;

    }

    public void add(int pos, T elem) {

        if (pos < 0 || pos > capacity) {
            throw new IndexOutOfBoundsException();
        }
        // Time to resize
        if (nItems + 1 >= capacity) {
            if (capacity == 0) {
                capacity = 1;
            } else {
                capacity *= 2; // double the size
            }
            Object[] tempArray = new Object[capacity];
            for (int i = 0; i < nItems; i++) {
                if (i == pos) {
                    tempArray[i] = elem;
                }
                tempArray[i] = array[i];
            }
            array = tempArray;
        }

        array[nItems++] = elem;

    }
    
    @Override
    public void push(T t) {
        if (isFull()) {
            return;
        }

        array[nItems] = t;
        nItems++;
    }

    @Override
    public T remove() {
        if (nItems == -1 || nItems == capacity) {
            return null;
        }

        Object temp = remove(0);

        return (T) temp;
    }

    // Removes an element at the specified index in this array. 
    public T remove(int index) {
        if (index > nItems && index < 0) {
            throw new IndexOutOfBoundsException();
        }
        Object data = array[index];
        Object[] tempArray = (T[]) new Object[capacity];
        for (int i = 0, j = 0; i < nItems; i++, j++) {
            if (i == index) {
                j--;
            } else {
                tempArray[j] = array[i];
            }
        }
        array = tempArray;
        nItems--;
        return (T) data;
    }

    // peek at front
    @Override
    public T element() {
        if (isEmpty()) {
            return null;
        }
        Object removed = array[0];
        return (T) removed;
    }
    
     /**
     * returns the top element of this array peek - returns a reference to the item at the size of the stack without removing it. Returns              null is the stack is empty.
     */
    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        }
        int temp = nItems - 1;
        return (T) array[temp];
    }
    
      /**
     * returns and removes the top element in this stack
     *
     * @return
     */
    @Override
    public T pop() {
        if (isEmpty()) {
            return null;
        }
        Object removed = array[--nItems];
        array[nItems] = null;
        return (T) removed;
    }




    @Override
    public String toString() {
        if (nItems == 0) {
            return "[]";
        } else {
            StringBuilder sb = new StringBuilder(nItems).append("");
            for (int i = 0; i < nItems - 1; i++) {
                sb.append(array[i] + "");
            }
            return sb.append(array[nItems - 1] + "").toString();
        }
    }

}
