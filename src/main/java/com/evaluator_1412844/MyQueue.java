/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluator_1412844;

//arrays.copy

/**
 * Structure that supports only two operations
Add member to the stack
Adds the member to the current end of the stack
Called “push”
Remove a member from the stack
Removes the last member added
Called “pop”
“Last In First Out” or LIFO structure

 */
/**
 *
 * @author ehugh
 */
public interface MyQueue<T> {

    public void add(T t); // add to end

    public T remove(); // remove from front

    public T element(); // peek at front

    public int size();
    

}
