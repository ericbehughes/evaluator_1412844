/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluator_1412844;

import com.customexceptions.InvalidOperatorPositionException;
import com.customexceptions.InvalidParenthesesException;
import com.customexceptions.InvalidPreFixException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehughes
 */
public class MyEvaluator {

    private MyQueue preFixExpressionQueue;
    private MyStack operatorStack;
    private MyQueue postFixQueue;
    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    public MyEvaluator(MyQueue expressionQueue) throws InvalidOperatorPositionException, 
            InvalidParenthesesException, InvalidPreFixException {
        this.preFixExpressionQueue = expressionQueue;
        operatorStack = new DynamicArray();
        postFixQueue = new DynamicArray();
        

        if (validatePreFix(preFixExpressionQueue)) {
            evaluateExpression();
        }

    }

    /**
     * algorithm
     *
     * Stack holds left parentheses and the operators only
     *
     * if char(i) is an number add to queue
     *
     * if char(i) is a '(' left parentheses push it on the stack
     *
     * if char(i) is a ')' right parentheses pop the stack and print the operators in the output result until you reach the left parenthesis
     *
     * if char(i) is an operator +-/ or '*' if stack isEmpty or contains '(' on top, push the incoming operator onto the stack
     *
     * if the incoming symbol has a higher precedence than the top of the stack, push it on the stack
     *
     * if th incoming symbol has a lower precendence than the symbol on the top of the stack, pop the stack and print.
     *
     *
     * example: (1 + 2 ) - 4 '*' 3 1. S: '(' + Q: 1 2
     *
     * 2. S: '(' + ')' Q: 1 2
     *
     * 3. S: Q: 3 - 4 * 3
     *
     * 4. S: - Q: 3 4 * 3
     *
     * 5. S: - * Q: 3 4 3
     *
     * 6. S: - Q: 12 3
     *
     * 7. S: -9
     *
     *
     *
     */
    private void evaluateExpression() throws InvalidPreFixException {

        //get size of prefix queue for loop
        int size = preFixExpressionQueue.size();
        for (int i = 0; i < size; i++) {
            String currentElement = (String) preFixExpressionQueue.remove();
            if (currentElement != null) {
                // check what string we are dealing with
                switch (currentElement.charAt(0)) {
                    case '/':
                    case '-':
                    case '*':
                    case '+':
                        // 
                        if (operatorStack.size() > 0) {
                            String topOfStackElement = (String) operatorStack.peek();
                            int rank1 = getRankOfOperator(currentElement.charAt(0));
                            int rank2 = getRankOfOperator(topOfStackElement.charAt(0));
                            if (rank1 < rank2) {
                                postFixQueue.add(operatorStack.pop());
                                operatorStack.push(currentElement);
                                break;

                            } else if (rank1 == rank2) {
                                String topOfStackTemp = (String) operatorStack.pop();
                                postFixQueue.add(topOfStackTemp);
                                operatorStack.push(currentElement);
                                break;

                            }
                        }

                        operatorStack.push(currentElement);

                        break;
                    case '(':
                        operatorStack.push(currentElement);
                        break;
                    case ')':

                        String operator = (String) operatorStack.pop();
                        postFixQueue.add(operator);
                        String topOfStackTemp = (String) operatorStack.pop();
                        while (topOfStackTemp.charAt(0) != '(') {
                            postFixQueue.add(topOfStackTemp);
                            topOfStackTemp = (String) operatorStack.pop();
                        }
                        break;
                    default:
                        postFixQueue.add(currentElement);
                        break;

                }
            }else{
                throw new InvalidPreFixException("invalid pre fix element");
            }

        }

        MyStack tempStack = new DynamicArray<>();

        // add remainder to postFix before calculation
        while (operatorStack.peek() != null) {
            String remainder = (String) operatorStack.pop();
            postFixQueue.add(remainder);

        }
         log.debug("    PostFix Queue " + postFixQueue.toString());
        // loop through postFix and calculate your result into a tempStack
        while (postFixQueue.element() != null) {
            String element = (String) String.valueOf(postFixQueue.remove());
            while (getRankOfOperator(element.charAt(0)) < 1) {
                tempStack.push(element);
                element = (String) postFixQueue.remove();

            }

            String b = (String) String.valueOf(tempStack.pop());
            String c = (String) String.valueOf(tempStack.pop());

            double result = computeExpression(c, b, element.charAt(0));
            int scale = (int) Math.pow(10, 2);
            result = (double) Math.round(result * scale) / scale;
            tempStack.push(result);

        }
        // add result back into postFixQueue and youre done the conversion
        postFixQueue.add(tempStack.pop());

    }

    //loop through queue until operator is found
    // compute result and clear stack i think
    private double computeExpression(String temp, String o, char c) {
        double result = 0;
        double a, b;

        a = Double.parseDouble(temp);
        b = Double.parseDouble(o);
        switch (c) {
            case '/':
                result = a / b;
                break;
            case '*':
                result = a * b;
                break;
            case '+':
                result = a + b;
                break;
            case '-':
                result = a - b;
                break;
        }
        return result;
    }

    private int getRankOfOperator(char c) {
        int rank = 0;
        switch (c) {
            case '/':
                rank = 4;
                break;
            case '*':
                rank = 3;
                break;
            case '+':
            case '-':
                rank = 1;
                break;
        }
        return rank;
    }

    @Override
    public String toString() {
        return postFixQueue.element().toString();
    }


    private boolean validatePreFix(MyQueue<String> prefixQueue) throws InvalidOperatorPositionException, InvalidParenthesesException {
        String s = prefixQueue.toString();
        int length = s.length();
        
        //check for duplicate ++ // and **
        Pattern p = Pattern.compile("([+*\\/])\\1");
        Matcher m = p.matcher(s);
        
        if (m.find()) {
            throw new InvalidOperatorPositionException("can't have 2 operators beside each other");
        }

        // check if start of string is an operator
        if (s.substring(0, 1).contains("+-/*")) {
            throw new InvalidOperatorPositionException("can't have operator at start of expression");
        }

        // check if end of string is operator
        if (s.substring(length - 1).contains("+-/*")) {
            throw new InvalidOperatorPositionException("can't have operator at end of expression");
        }

       
        // check if parenthesis are beside one another
        int leftP = s.indexOf("(");
        int rightP = s.indexOf(")");

        if ((leftP > 0 && rightP < 0) || (leftP < 0 && rightP > 0)) {
            throw new InvalidParenthesesException("missing matching parentheses");
        } else {
            
            while (leftP != -1 || rightP != -1) {
                if (leftP + 1 == rightP || rightP + 1 == leftP) {
                    throw new InvalidParenthesesException("cant have mis match parenthesis beside each other");
                } else {
                    leftP = s.indexOf("(", leftP + 1);
                    rightP = s.indexOf(")", rightP + 1);
                }
            }
        }

        /**
         * check if count of parenthesis is same
         */
        boolean isEven = true;
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                count++;
            }
            if (s.charAt(i) == ')') {
                count--;
            }
            if (count < 0) { // ) with no (
                isEven = false;
                break;
            }
        }
        if (count > 0) {
            isEven = false; // ( with no )
        }
        if (!isEven) {
            throw new InvalidParenthesesException("invalid number of matching parenthesis");
        }
        return true;

    }

}
