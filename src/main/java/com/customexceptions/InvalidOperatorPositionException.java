/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.customexceptions;

/**
 *
 * @author ehugh
 */
public class InvalidOperatorPositionException   extends Exception{
     public InvalidOperatorPositionException(String error) {
            super(error);
        }
}
