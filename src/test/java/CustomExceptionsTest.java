
import com.evaluator_1412844.DynamicArray;
import com.evaluator_1412844.MyEvaluator;
import com.evaluator_1412844.MyQueue;
import com.customexceptions.InvalidOperatorPositionException;
import com.customexceptions.InvalidParenthesesException;
import com.customexceptions.InvalidPreFixException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ehugh
 */
public class CustomExceptionsTest {
      private MyQueue q;
    

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());

    /**
     * Constructor that receives all the data for each test as defined by a row in the list of parameters
     *
     */
    public CustomExceptionsTest() {
        
    }
    
    
    /**
     *
     * @throws com.customexceptions.InvalidParenthesesException
     */
    @Test (expected = InvalidParenthesesException.class)
    public void invalidParenthesesCount() throws InvalidParenthesesException, InvalidOperatorPositionException, InvalidPreFixException {
        
        String[] expression = new String[]{"(", "(", "+", "2", ")", "-", "4", "*", "3"};
        this.q = new DynamicArray();
        for (String string : expression) {
            this.q.add(string);
        }
        
        MyEvaluator e = null;
        
            e = new MyEvaluator(this.q);

        
        fail("InvalidParenthesesException");

    }
    
    
    
     @Test (expected = InvalidOperatorPositionException.class)
    public void invalidOperatorPosition() throws InvalidParenthesesException, InvalidOperatorPositionException, InvalidPreFixException {
        
        String[] expression = new String[]{"+", "+", "2", "-", "4", "*", "3"};
        this.q = new DynamicArray();
        for (String string : expression) {
            this.q.add(string);
        }
        
        MyEvaluator e = null;
            e = new MyEvaluator(this.q);

        fail("InvalidOperatorPositionException");

    }
    
     @Test (expected = InvalidPreFixException.class)
    public void invalidPreFixException() throws InvalidParenthesesException,InvalidOperatorPositionException, InvalidPreFixException {
        
        String[] expression = new String[]{"(", null, "+", "2", ")", "-", "4", "*", "3"};
        this.q = new DynamicArray();
        for (String string : expression) {
            this.q.add(string);
        }
        
        MyEvaluator e = null;
        try {
            e = new MyEvaluator(this.q);

        } catch (InvalidOperatorPositionException ex) {
            java.util.logging.Logger.getLogger(ex.getMessage());
        } 

        fail("InvalidPreFixException");

    }
    
    
    
}
