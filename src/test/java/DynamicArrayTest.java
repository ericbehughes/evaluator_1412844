

/**
 *
 * @author ehughes
 */
import com.evaluator_1412844.DynamicArray;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DynamicArrayTest {

    @Test
    public void testEmptyList() {
        DynamicArray<Integer> list = new DynamicArray<>();
        assertTrue(list.isEmpty());
    }



    @Test(expected = Exception.class)
    public void testIndexOutOfBounds() {
        DynamicArray<Integer> list = new DynamicArray<>();
        for (int i = 0; i < 100; i++) {
            list.add(4);
        }
        list.remove(-1);
    }


    @Test
    public void testRemoving() {

        DynamicArray<String> list = new DynamicArray<>();
        String[] strs = {"bob", "cab", "cat", "denis", "eric", null, "geoge", "hughes"};
        for (String s : strs) {
            list.add(s);
        }

        Object ret = list.remove(2);
        assertTrue(ret.equals(strs[2]));

    }


    @Test
    public void testAddingElements() {

        DynamicArray<Integer> list = new DynamicArray<>();

        int[] elems = {1, 2, 3, 4, 5, 6, 7};

        for (int i = 0; i < elems.length; i++) {
            list.add(elems[i]);
        }

        for (int i = 0; i < elems.length; i++) {
            assertEquals(list.get(i).intValue(), elems[i]);
        }

    }

    @Test
    public void testAddAndRemove() {

        DynamicArray<Double> list = new DynamicArray<>(0);

        for (int i = 0; i < 99; i++) {
            list.add(22.0);
        }
        for (int i = 0; i < 99; i++) {
            list.remove(44);
        }
        assertTrue(list.isEmpty());

        for (int i = 0; i < 99; i++) {
            list.add(22.0);
        }
        for (int i = 0; i < 99; i++) {
            list.remove(0);
        }
        assertTrue(list.isEmpty());

        for (int i = 0; i < 60; i++) {
            list.add(22.0);
        }
        for (int i = 0; i < 60; i++) {
            list.remove(44);
        }
        assertTrue(list.isEmpty());

        for (int i = 0; i < 60; i++) {
            list.add(22.0);
        }
        for (int i = 0; i < 60; i++) {
            list.remove(0);
        }
        assertTrue(list.isEmpty());

    }

    @Test
    public void testAddSetRemove() {

        DynamicArray<Double> list = new DynamicArray<>(0);

        for (int i = 0; i < 99; i++) {
            list.add(22.0);
        }
        for (int i = 0; i < 99; i++) {
            list.set(i, 33.0);
        }
        for (int i = 0; i < 99; i++) {
            list.remove(33);
        }
        assertTrue(list.isEmpty());

        for (int i = 0; i < 99; i++) {
            list.add(22.0);
        }
        for (int i = 0; i < 99; i++) {
            list.set(i, 33.0);
        }
        for (int i = 0; i < 99; i++) {
            list.remove(0);
        }
        assertTrue(list.isEmpty());

        

    }

}
