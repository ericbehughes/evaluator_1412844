
import com.evaluator_1412844.DynamicArray;
import com.evaluator_1412844.MyEvaluator;
import com.evaluator_1412844.MyQueue;
import com.customexceptions.InvalidOperatorPositionException;
import com.customexceptions.InvalidParenthesesException;
import com.customexceptions.InvalidPreFixException;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import static org.junit.Assert.assertEquals;
import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A parameterized test class.
 *
 * @author eric hughes
 */
@RunWith(Parameterized.class)
public class MyEvaluatorTest {

    private MyQueue q;
    private String[] array;
    private String result;

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());

    /**
     * Constructor that receives all the data for each test as defined by a row in the list of parameters
     *
     */
    public MyEvaluatorTest(String[] array, String result) {
        this.array = array;
        this.result = result;
    }

    /**
     *
     * @return The Queues of math expressions from an array of string
     */
    @Parameterized.Parameters//(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            //1. (1+2) - 4 * 3 = -9.0
            {new String[]{"(", "1", "+", "2", ")", "-", "4", "*", "3"}, "-9.0"},
            //2.  2*(3+2+2)*3  = 42.0
            {new String[]{"2", "*", "(", "3", "+", "2", "+", "2", ")", "*", "3"}, "42.0"},
            //3.  6*(3+2)+3-1 = 32
            {new String[]{"8", "*", "2", "-", "(", "1", "*", "6", "+", "12", ")"}, "-2.0"},
            //4. 6*(3+2)+3-1 = 32.0
            {new String[]{"6", "*", "(", "3", "+", "2", ")", "+", "3", "-", "1"}, "32.0"},
            //5. 8 + 6 / 2 (1 - 7 ) + 2 ( 10 * 2) = 30
            {new String[]{"8", "+", "6", "/", "2", "*", "(", "1", "-", "7", ")", "+", "2", "*", "(", "10", "*", "2", ")"}, "30.0"},
            //6. (3-6)*1+1
            {new String[]{"(", "3", "-", "6", ")", "*", "1", "+", "1"}, "-2.0"},
            //7. 4*4*2 + 19 ( 1+ 1) = 70.0
            {new String[]{"4", "*", "4", "*", "2", "+", "19", "*", "(", "1", "+", "1", ")"}, "70.0"},
            //8. 1+2 * 2+4 - 1-100 = -92
            {new String[]{"1", "+", "2", "*", "2", "+", "4", "-", "1", "-", "100"}, "-92.0"},
            //9. 1+2 * 2+4 - 1-100 * ( 7 + 7) -1 = -1393
            {new String[]{"1", "+", "2", "*", "2", "+", "4", "-", "1", "-", "100", "+", "(", "7", "+", "7", ")", "-", "1"}, "-79.0"},
            //10. 50 - 18 + ( 1 /2 + 4) + 7 + 9 = 52.5
            {new String[]{"50", "-", "18", "+", "(", "1", "/", "2", "+", "4", ")", "+", "7", "+", "9"}, "52.5"},
            //11. 18 + ( 16/ 4 ) + 17 + 17 = 39.0
            {new String[]{"18", "+", "(", "16", "/", "4", ")", "+", "17"}, "39.0"},
            //12. 9-7-6-5-4-3-2-1 = -19
            {new String[]{"9", "-", "7", "-", "6", "-", "5", "-", "4", "-", "3", "-", "2", "-", "1"}, "-19.0"},
            //13. (1+1) * (2*2) * (3 * 3) = 72
            {new String[]{"(", "1", "+", "1", ")", "*", "(", "2", "*", "2", ")", "*", "(", "3", "*", "3", ")"}, "72.0"},
            //14. 6 * 7 ( 6+ 1) +9 = 429
            {new String[]{"6", "*", "7", "*", "(", "6", "+", "1", ")", "+", "9"}, "303.0"},
            //15. (90 / 4) + ( -60 + 14) = -1035
            {new String[]{"(", "90", "/", "4", ")","+","(", "60", "+", "14", ")"}, "96.5"},
            //16. 9 / 3 + 7 - 4 + 1 + 1 - 88 = -80
            {new String[]{ "9", "/", "3", "+", "7", "-", "4", "+", "1", "+", "1", "-", "88"}, "-80.0"},
            //17. 3 * 3 - 17 + 4  - 88 = -155
            {new String[]{"3", "*", "3", "-", "17", "+", "4", "-", "88"}, "-92.0"},
            //18. (7 + 7) - 4 * ( 1 + 11 / 2) = -12
            {new String[]{"(", "7", "+", "7", ")", "-", "4", "*", "(", "1", "+", "11", "/", "2", ")"}, "-12.0"},
            //19. 11 * 11 - 7 + 5 / 2 = 116.5
            {new String[]{"11", "*", "11", "-", "7", "+", "5", "/", "2"}, "116.5"},
            //20. 4-3 + (3+2*9) = 22
            {new String[]{"4", "-", "3", "+", "(", "3", "+", "2", "*", "9", ")"}, "22.0"},});
    }

    @Before
    public void createQueueFromString() {
        this.q = new DynamicArray();
        for (String string : array) {
            this.q.add(string);
        }
    }

    /**
     *
     */
    @Test
    public void testEvaluator1() {
        MyEvaluator e = null;
        log.debug("PreFix Queue " + this.q.toString());
        try {
            e = new MyEvaluator(this.q);

        } catch (InvalidOperatorPositionException ex) {
            java.util.logging.Logger.getLogger(ex.getMessage());
        } catch (InvalidParenthesesException ex) {
            java.util.logging.Logger.getLogger(ex.getMessage());
        } catch (ArithmeticException ae) {
            java.util.logging.Logger.getLogger(ae.getMessage());
        } catch (InvalidPreFixException ex) {
            java.util.logging.Logger.getLogger(MyEvaluatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (e != null) {
            log.debug("Postfix calculation= " + e.toString());
            log.debug("==================== ");
            assertEquals(this.result, e.toString());
        }

    }

}
